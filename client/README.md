# Setup
Run `npm install` to install dependencies

Then create `.env` file with value:
```
VUE_APP_BASE_URL=http://localhost:3000
```

Then run `npm run serve` to start